function geoFindMe() {

    let status = document.querySelector('#status');
    let mapLink = document.querySelector('#map-link');

    mapLink.href = '';
    mapLink.textContent = '';

    function success(position) {
        let lat = position.coords.latitude;
        let long = position.coords.longitude;
       
        status.textContent = '';
        mapLink.href = `https://www.openstreetmap.org/#map=18/${lat}/${long}`;
        mapLink.textContent = `Latitude: ${lat} °, Longitude: ${long} °`;

        constructURL(lat, long);
        console.log(lat, long);
        
    }

    function error() {
        status.textContent = 'Unable to retrieve your location';
    }

    if (!navigator.geolocation) {
        status.textContent = 'Geolocation is not supported by your browser';
    } else {
        status.textContent = 'Locating…';
        navigator.geolocation.getCurrentPosition(success, error);
    }

}
document.querySelector('#find-me').addEventListener('click', geoFindMe);



function constructURL(lat, long) {
    // console.log(lat, long);
    
    fetch("https://shrouded-mountain-15003.herokuapp.com/https://www.flickr.com/services/rest/?api_key=97da67bb5bfda7d67183a108e18e4dcb&format=json&nojsoncallback=1&method=flickr.photos.search&safe_search=1&per_page=5&lat=" + lat + "&lon=" + long + "&text=praça")
        .then(responseObj => {
            return responseObj.json();
        })
        .then(jsonObj => {
            photosObj = jsonObj;
            // console.log(photosObj);   
            let img = document.getElementById('img');
            img.src = constructImageURL(photosObj.photos.photo[i]);           
                    
        });
}

function constructImageURL(photoObj) {
    return "https://farm" + photoObj.farm +
        ".staticflickr.com/" + photoObj.server +
        "/" + photoObj.id + "_" + photoObj.secret + ".jpg";
}


var i = 0;
function proxImg() {
    i++;
    if (i === 0 || i <= 4) {
        let img = document.getElementById('img');
        img.src = constructImageURL(photosObj.photos.photo[i]);
        
    }
    if (i > 4) {
        i = 0;
        let img = document.getElementById('img');
        img.src = constructImageURL(photosObj.photos.photo[i]);
        
    }
}
document.getElementById('next').addEventListener('click', proxImg);


